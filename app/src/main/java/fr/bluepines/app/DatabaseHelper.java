package fr.bluepines.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.InputStream;
import java.sql.SQLException;

import fr.bluepines.app.models.Exercise;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.models.Program_exercise;
import fr.bluepines.app.models.User_infos;

/**
 * Created by gecko on 11/06/14.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "BluePines.sqlite";
    private static final int DB_VERSION = 3;


    private Dao<Program, Integer> programDao = null;
    private Dao<User_infos, Integer> userInfosDao = null;
    private Dao<Exercise, Integer> exerciseDao = null;
    private Dao<Program_exercise, Integer> program_exerciseDao = null;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Program.class);
            TableUtils.createTable(connectionSource, User_infos.class);
            TableUtils.createTable(connectionSource, Exercise.class);
            TableUtils.createTable(connectionSource, Program_exercise.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, Program.class, true);
            TableUtils.dropTable(connectionSource, User_infos.class, true);
            TableUtils.dropTable(connectionSource, Exercise.class, true);
            TableUtils.dropTable(connectionSource, Program_exercise.class, true);
            onCreate(sqLiteDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // DAO getters
    public Dao<Program, Integer> getProgramDao() {
        if (programDao == null) {
            try {
                programDao = getDao(Program.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return programDao;
    }
    public Dao<User_infos, Integer> getUserInfosDao() {
        if (userInfosDao == null) {
            try {
                userInfosDao = getDao(User_infos.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userInfosDao;
    }
    public Dao<Exercise, Integer> getExerciseDao() {
        if (exerciseDao == null) {
            try {
                exerciseDao = getDao(Exercise.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return exerciseDao;
    }
    public Dao<Program_exercise, Integer> getProgram_exerciseDao() {
        if (program_exerciseDao == null) {
            try {
                program_exerciseDao = getDao(Program_exercise.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return program_exerciseDao;
    }
}
