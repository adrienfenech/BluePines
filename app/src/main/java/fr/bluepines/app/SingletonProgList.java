package fr.bluepines.app;

import android.text.format.Time;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import fr.bluepines.app.models.Exercise;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.models.Seance;
import fr.bluepines.app.models.Set;

/**
 * Created by Jérémie on 04/05/2014.
 * Accessibility of the program list from everywhere with this singleton
 * Just call SingletonProgList.programs
 */
public class SingletonProgList {
    private static SingletonProgList instance = null;
    public static List<Program> programs = new ArrayList<Program>();
    public static List<Seance> seances = new ArrayList<Seance>();

    public static List<Pair<Time, Exercise>> done;

    protected SingletonProgList() {
        // Exists only to defeat instantiation.
    }

    public static SingletonProgList getInstance() {
        if (instance == null) {
            instance = new SingletonProgList();
            done = new ArrayList<Pair<Time, Exercise>>();
        }
        return instance;
    }

    public static void copy(List<Program> newList) {
        programs = newList;

        /* For testing purposes */
        Program p1 = new Program("Test0", "Retest0");
        p1.addExo(new Exercise("Pompes collées", "Epaules", new Set(15, 0)));
        p1.addExo(new Exercise("Pompes classiques", "Biceps", new Set(15, 0)));
        p1.addExo(new Exercise("Planche", "Dos", new Set(60, 0)));

        Program p2 = new Program("Test1", "Retest1");
        p2.addExo(new Exercise("Développé couché", "Pectoraux", new Set(10, 20)));
        p2.addExo(new Exercise("Lift", "Biceps", new Set(10, 20)));
        p2.addExo(new Exercise("Traction", "Triceps", new Set(10, 0)));

        Program p3 = new Program("Lol", "NoUse");
        p3.addExo(new Exercise("Rien", "Rien non plus", new Set(12, 27)));
        p3.addExo(new Exercise("Toujours rien", "Vide", new Set(4, 8)));
        p3.addExo(new Exercise("Neant", "Un lapin", new Set(20, 17)));

        programs.add(p1);
        programs.add(p2);
        programs.add(p3);
    }

    public static void copySeance(List<Seance> newList) {
        seances = newList;
        seances.add(new Seance(new ArrayList<Program>(), "Séance libre"));
    }

    public static void addDone(Exercise exo) {
        Time now = new Time();
        now.setToNow();
        done.add(new Pair(now, exo));
    }

    public static void newSeance(Seance seance) {
        Seance s = new Seance(seance.getPgList(), seance.getName());
        seances.add(s);
        // Add the new seance in the DB
    }
}
