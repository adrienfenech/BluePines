package fr.bluepines.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Gecko on 01/06/14.
 */
public class AllExercises {
    // Progress Dialog
    private ProgressDialog pDialog;
    private Activity parent;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> exercisesList;

    // url to get all exercises list
    private static String url_all_exercises = "http://apibluepines.gecko-splinter.fr/get_exercises.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_EXERCISES = "exercises";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESC = "description";
    private static final String TAG_UNIT = "unit";

    // exercises JSONArray
    JSONArray exercises = null;

    public AllExercises(Activity parent) {

        // Hashmap for ListView
        exercisesList = new ArrayList<HashMap<String, String>>();

        this.parent = parent;

        // Loading exercises in Background Thread
        new LoadAllExercises().execute();

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllExercises extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(parent);
            pDialog.setMessage("Loading exercises. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All exercises from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_exercises, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // exercises found
                    // Getting Array of Products
                    exercises = json.getJSONArray(TAG_EXERCISES);

                    // looping through All Products
                    for (int i = 0; i < exercises.length(); i++) {
                        JSONObject c = exercises.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);

                        // adding HashList to ArrayList
                        exercisesList.add(map);
                    }
                } else {
                    // no exercises found
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all exercises
            pDialog.dismiss();
        }

    }
}
