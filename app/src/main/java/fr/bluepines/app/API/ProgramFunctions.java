package fr.bluepines.app.API;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gecko on 02/05/14.
 */
public class ProgramFunctions {
    private JSONParser jsonParser;

    // Testing in localhost using wamp or xampp
    // use http://10.0.2.2/ to connect to your localhost ie http://localhost/
    private static String apiURL = "http://apibluepines.gecko-splinter.fr";

    private static String sync_tag = "sync";
    private static String create_tag = "create";

    // constructor
    public ProgramFunctions(){
        jsonParser = new JSONParser();
    }

    /**
     * function make Program sync Request
     * @param name
     * */
    public JSONObject getProgram(String name){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", sync_tag));
        params.add(new BasicNameValuePair("name", name));
        JSONObject json = jsonParser.getJSONFromUrl(apiURL, params);
        // return json
        // Log.e("JSON", json.toString());
        return json;
    }

    /**
     * function make Login Request
     * @param name
     * @param description
     * */
    public JSONObject createProgram(String name, String description){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", create_tag));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("description", description));

        // getting JSON Object
        JSONObject json = jsonParser.getJSONFromUrl(apiURL, params);
        // return json
        return json;
    }
}
