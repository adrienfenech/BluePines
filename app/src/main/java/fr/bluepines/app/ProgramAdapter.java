package fr.bluepines.app;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.bluepines.app.models.Program;

/**
 * Created by TaTa_HenRieTTe on 24/04/14.
 */
public class ProgramAdapter extends ArrayAdapter<Program> {

    private final List<Program> programs;
    private final Activity context;

    static class ViewHolder {
        TextView programName;
        TextView programDescription;
        CheckBox checkBox;
    }

    public ProgramAdapter(Activity context, List<Program> programs) {
        super(context, R.layout.formation_list_adapter, programs);
        this.programs = programs;
        this.context = context;
    }

    public void updateProgram(Program program) {
        for (Program p : programs) {
            if (p.getId() == program.getId()) {
                p.setDescription(program.getDescription());
                p.setName(program.getName());
            }
        }
    }

    public List<Program> getSelected() {
        List<Program> result = new ArrayList<Program>();
        for (Program p : programs)
            if (p.isSelected())
                result.add(p);

        return result;
    }

    public List<Integer> getSelectedInts() {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < programs.size(); i++)
            if (programs.get(i).isSelected())
                result.add(i);

        return result;
    }

    public void reinitCheck() {
        for (Program p : programs)
            p.setSelected(false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            final ViewHolder holder = new ViewHolder();
            view = inflater.inflate(R.layout.formation_list_adapter, null);

            holder.programName = (TextView) view.findViewById(R.id.name);
            holder.programDescription = (TextView) view.findViewById(R.id.description);
            holder.checkBox = (CheckBox) view.findViewById(R.id.check);
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    Program element = (Program) holder.checkBox.getTag();
                    element.setSelected(buttonView.isChecked());

                }
            });

            view.setTag(holder);
            holder.checkBox.setTag(programs.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkBox.setTag(programs.get(position));
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.programName.setText(programs.get(position).getName());
        holder.programDescription.setText(programs.get(position).getDescription());
        holder.checkBox.setChecked(programs.get(position).isSelected());
        return view;
    }
}
