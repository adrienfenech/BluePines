package fr.bluepines.app;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.bluepines.app.models.Seance;

/**
 * Created by Jérémie on 31/05/2014.
 */
public class SeanceAdapter extends ArrayAdapter<Seance> {
    private final Activity context;
    private final List<Seance> seances;

    public SeanceAdapter(Activity context, List<Seance> sea) {
        super(context, R.layout.classic_list, sea);
        this.seances = sea;
        this.context = context;
    }

    static class ViewHolder {
        TextView name;
        TextView size;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            final ViewHolder holder = new ViewHolder();
            view = inflater.inflate(R.layout.classic_list, null);

            holder.name = (TextView) view.findViewById(R.id.name);
            holder.size = (TextView) view.findViewById(R.id.description);

            view.setTag(holder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(seances.get(position).getName());
        holder.size.setText("Nombre d'exercices : " + seances.get(position).getSize());

        return view;
    }
}
