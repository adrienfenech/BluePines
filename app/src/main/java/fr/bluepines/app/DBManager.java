package fr.bluepines.app;

import android.content.Context;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import fr.bluepines.app.models.Exercise;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.models.Program_exercise;
import fr.bluepines.app.models.User_infos;

/**
 * Created by gecko on 11/06/14.
 */
public class DBManager {
    static private DBManager instance;

    static public void init(Context ctx) {
        if (null==instance) {
            instance = new DBManager(ctx);
        }
    }

    static public DBManager getInstance() {
        return instance;
    }

    private DatabaseHelper helper;
    private DBManager(Context ctx) {
        helper = new DatabaseHelper(ctx);
    }

    private DatabaseHelper getHelper() {
        return helper;
    }

    // Program methods
    public List<Program> getAllPrograms() {
        List<Program> programs = null;
        try {
            programs = getHelper().getProgramDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (programs == null)
            programs = new ArrayList<Program>();
        return programs;
    }
    public void createProgram(Program p) {
        try {
            getHelper().getProgramDao().create(p);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removePrograms(Collection<Program> list) {
        try {
            getHelper().getProgramDao().delete(list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateProgram(Program p) {
        try {
            getHelper().getProgramDao().update(p);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // User infos methods
    public User_infos getUserInfos(int id) {
        User_infos uinfo = null;
        try {
            uinfo = getHelper().getUserInfosDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return uinfo;
    }

    public List<Exercise> getAllExercises() {
        List<Exercise> exercises = null;
        try {
            exercises = getHelper().getExerciseDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (exercises == null)
            exercises = new ArrayList<Exercise>();
        return exercises;
    }
    public void initExercises() {
        List<Exercise> exerciseList = null;
        try {
            exerciseList = getHelper().getExerciseDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (exerciseList == null) {
            exerciseList = new ArrayList<Exercise>();
            Random rn = new Random();

            for (int i = 0; i < 10; i++) {
                int a = rn.nextInt() % 4;
                exerciseList.add(new Exercise("Exo " + i, "Description " + i, "Unit" + a, "Category " + a));
            }
            try {
                for (Exercise e : exerciseList)
                    getHelper().getExerciseDao().create(e);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Program exercise methods
    public List<Program_exercise> getProgram_exercises(Program p) {
        List<Program_exercise> program_exercises = null;

        return program_exercises;
    }
}
