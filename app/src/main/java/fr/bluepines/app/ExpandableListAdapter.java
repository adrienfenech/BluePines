package fr.bluepines.app;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import fr.bluepines.app.activities.ModificationActivity;

/**
 * Created by TaTa_HenRieTTe on 02/06/2014.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> headerDataList;
    private HashMap<String, List<String>> itemDataList;

    public ExpandableListAdapter(ModificationActivity context, List<String> headerDataList, HashMap<String, List<String>> itemDataList) {
        this.context = context;
        this.headerDataList = headerDataList;
        this.itemDataList = itemDataList;
    }
    @Override
    public Object getChild(int headPosition, int itemPosition) {
        return this.itemDataList.get(this.headerDataList.get(headPosition)).get(itemPosition);
    }
    @Override
    public long getChildId(int headPosition, int itemPosition) {
        return itemPosition;
    }
    @Override
    public View getChildView(int headPosition, final int itemPosition, boolean isLastItem, View convertView, ViewGroup parent) {
        final String itemText = (String) getChild(headPosition, itemPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.dialog_list_item, null);
        }
        TextView txtItemList = (TextView) convertView.findViewById(R.id.expandableListItem);
        txtItemList.setText(itemText);

        return convertView;
    }
    @Override
    public int getChildrenCount(int headPosition) {
        return this.itemDataList.get(this.headerDataList.get(headPosition)).size();
    }
    @Override
    public Object getGroup(int headPosition) {
        return this.headerDataList.get(headPosition);
    }

    public int getGroupCount() {
        return this.headerDataList.size();
    }
    @Override
    public long getGroupId(int headPosition) {
        return headPosition;
    }
    @Override
    public View getGroupView(int headPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(headPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.dialog_list_group, null);
        }
        TextView expandHeaderList = (TextView) convertView.findViewById(R.id.expandableListHeader);
        expandHeaderList.setTypeface(null, Typeface.BOLD);
        expandHeaderList.setText(headerTitle);

        return convertView;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public boolean isChildSelectable(int headPosition, int itemPosition) {
        return true;
    }
}
