package fr.bluepines.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TaTa_HenRieTTe on 24/04/14.
 */
@DatabaseTable(tableName = "exercises")
public class Exercise implements Parcelable {

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "name")
    private String name;
    @DatabaseField(columnName = "description")
    private String description;
    @DatabaseField(columnName = "unit")
    private String unit;
    @DatabaseField(columnName = "category")
    private String category;

    private Set current;
    private List<Set> setList;

    private int nbExoRepetition;
    private boolean selected;

    public Exercise() {}

    public Exercise(String name, String description, String unit, String category) {
        this.name = name;
        this.description = description;
        this.unit = unit;
        this.category = category;
    }

    public Exercise(String name, String description, String unit) {
        this.name = name;
        this.description = description;
        setList = new ArrayList<Set>();
        nbExoRepetition = 0;
    }

    public Exercise(String name, String description, Set todo) {
        this.name = name;
        this.description = description;
        current = todo;
    }

    public void setSet(Set set) {
        this.current = set;
    }

    // Getters and Setters
    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public long getId() { return id; }
    public String getName() { return name; }
    public String getDescription() { return description; }
    public String getUnit() { return unit; }
    public String getCategory() { return category; }

    public Exercise clone() {
        return new Exercise(getName(), getDescription(), new Set(current.getIteration(), current.getValue()));
    }

    public int getIter() {
        return current.getIteration();
    }
    public Set getCurrent() {
        return current;
    }
    public void setIter(int n) {
        current.setIteration(n);
    }
    public int getValue() {
        return current.getValue();
    }
    public void setValue(int n) {
        current.setValue(n);
    }
    public void add(int value, int iteration) {
        Set set = new Set(iteration, value);
        nbExoRepetition += 1;
        setList.add(set);
    }

    public int getNbExoRepetition() { return nbExoRepetition; }
    public Set getLastPair() {
        if (nbExoRepetition > 0)
            return setList.get(nbExoRepetition - 1);
        return null;
    }

    public Set getLastLastPair() {
        if (nbExoRepetition > 1)
            return setList.get(nbExoRepetition - 2);
        return null;
    }

    public boolean isSelected() {
        return selected;
    }

    /*
    ** Parcelable implementation
    */

    public int describeContents() {
        return 0;
    }

    /**
     * This is a must have to implement Parcelable, this is the function used to write data in the stream
     *
     * @param out   The parcel to be written in
     * @param flags don't know
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeList(setList);
        out.writeInt(nbExoRepetition);
        out.writeString(description);
        out.writeString(name);

        if (current != null)
            out.writeValue(current);
    }

    /**
     * Given a Parcel, this is the function used to get the original object back
     *
     * @param in The Parcel containing the original object
     */
    private Exercise(Parcel in) {
        setList = in.readArrayList(Set.class.getClassLoader());
        nbExoRepetition = in.readInt();
        description = in.readString();
        name = in.readString();

        if (in.dataAvail() > 0)
            current = (Set) in.readValue(Set.class.getClassLoader());
    }

    /**
     * Special structure, used to implement parcelable
     */
    public static final Parcelable.Creator<Exercise> CREATOR = new Parcelable.Creator<Exercise>() {
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }

        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };
}