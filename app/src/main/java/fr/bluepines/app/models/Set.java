package fr.bluepines.app.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gecko on 01/06/14.
 */
public class Set implements Parcelable {
    private long id;
    private int iteration;
    private int value;

    public Set(int iteration, int value) {
        this.iteration = iteration;
        this.value = value;
    }

    // GETTERS AND SETTERS
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int getIteration() {
        return iteration;
    }
    public void setIteration(int iteration) {
        this.iteration = iteration;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

/*
    ** Parcelable implementation
    */

    public int describeContents() {
        return 0;
    }

    /**
     * This is a must have to implement Parcelable, this is the function used to write data in the stream
     *
     * @param out   The parcel to be written in
     * @param flags don't know
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(iteration);
        out.writeInt(value);
    }

    /**
     * Given a Parcel, this is the function used to get the original object back
     *
     * @param in The Parcel containing the original object
     */
    private Set(Parcel in) {
        iteration = in.readInt();
        value = in.readInt();
    }

    /**
     * Special structure, used to implement parcelable
     */
    public static final Parcelable.Creator<Set> CREATOR = new Parcelable.Creator<Set>() {
        public Set createFromParcel(Parcel in) {
            return new Set(in);
        }

        public Set[] newArray(int size) {
            return new Set[size];
        }
    };
}
