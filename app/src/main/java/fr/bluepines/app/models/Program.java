package fr.bluepines.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TaTa_HenRieTTe on 24/04/14.
 */
@DatabaseTable(tableName = "programs")
public class Program implements Parcelable {

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "name", canBeNull = false)
    private String name;
    @DatabaseField(columnName = "description", canBeNull = false)
    private String description;
    @ForeignCollectionField
    private ForeignCollection<Program_exercise> program_exercises;

    private List<Exercise> exerciseList;

    private boolean selected;

    public Program() {
    }

    public Program(String name, String description) {
        this.name = name;
        this.description = description;
        this.selected = false;
        exerciseList = new ArrayList<Exercise>();
    }

    public String getName() { return name; }
    public String getDescription() { return description; }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public List<Program_exercise> getProgram_exercises() {
        List<Program_exercise> programExercises = new ArrayList<Program_exercise>();
        for (Program_exercise pe : program_exercises)
            programExercises.add(pe);
        return programExercises;
    }
    public void setProgram_exercises(ForeignCollection<Program_exercise> program_exercises) {
        this.program_exercises = program_exercises;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<Exercise> getExerciseList()
    {
       if (exerciseList == null)
           exerciseList = new ArrayList<Exercise>();
       return exerciseList;
    }
    public void initExerciseList() { exerciseList = new ArrayList<Exercise>(); }

    /**
     * @param new_exo The new exercice to add. We do not check if already exists.
     */
    public void addExo(Exercise new_exo)
    {
        if (exerciseList == null)
            exerciseList = new ArrayList<Exercise>();
        exerciseList.add(new_exo.clone());
    }

    public void addExo(int p, Exercise new_exo)
    {
        if (exerciseList == null)
            exerciseList = new ArrayList<Exercise>();
        exerciseList.add(p, new_exo);
    }

    public void addExos(List<Exercise> new_exos)
    {
        if (exerciseList == null)
            exerciseList = new ArrayList<Exercise>();
        exerciseList.addAll(new_exos);
    }

    /**
     * @param to_go Exercise to remove. Will exist as far as the user will chose it in a list
     */
    public void removeExo(Exercise to_go)
    {
        exerciseList.remove(to_go);
    }

    /**
     * @param i Place of the exercise
     */
    public Exercise getExo(int i)
    {
        if (i < 0 || i >= exerciseList.size())
            return null;
        return exerciseList.get(i);
    }

    public Program clone(){
        Program p = new Program(getName(), getDescription());
        for (Exercise e : exerciseList)
            p.addExo(e);
        return p;
    }

    /**
     * @param ex Exercise to find
     */
    public int getExoPlace(Exercise ex)
    {
        return exerciseList.indexOf(ex);
    }

    public int describeContents() {
        return 0;
    }

    /**
     * This is a must have to implement Parcelable, this is the function used to write data in the stream
     * @param out The parcel to be written in
     * @param flags don't know
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(name);
        out.writeString(description);
        out.writeLong(id);
        out.writeValue(selected);
        out.writeList(exerciseList);
    }

    /**
     * Given a Parcel, this is the function used to get the original object back
     * @param in The Parcel containing the original object
     */
    private Program(Parcel in) {
        name = in.readString();
        description = in.readString();
        id = in.readLong();
        selected = (Boolean)in.readValue(boolean.class.getClassLoader());
        exerciseList = in.readArrayList(Exercise.class.getClassLoader());
    }
    /**
     * Special structure, used to implement parcelable
     */
    public static final Parcelable.Creator<Program> CREATOR = new Parcelable.Creator<Program>() {
        public Program createFromParcel(Parcel in) {
            return new Program(in);
        }

        public Program[] newArray(int size) {
            return new Program[size];
        }
    };
}
