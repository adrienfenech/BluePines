package fr.bluepines.app.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Gecko on 01/06/14.
 */
@DatabaseTable(tableName = "user_infos")
public class User_infos {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "weight")
    private int weight;
    @DatabaseField(columnName = "height")
    private int height;
    @DatabaseField(columnName = "age")
    private int age;
    @DatabaseField(columnName = "sex")
    private String sex;

    public User_infos() { }

    public User_infos(int weight, int height, int age, String sex) {
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.sex = sex;
    }

    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
}
