package fr.bluepines.app.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Gecko on 01/06/14.
 */
@DatabaseTable(tableName = "users")
public class User {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "email")
    private String email;
    @DatabaseField(columnName = "password")
    private String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
