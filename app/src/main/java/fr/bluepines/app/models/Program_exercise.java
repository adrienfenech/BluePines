package fr.bluepines.app.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by gecko on 12/06/14.
 */
@DatabaseTable(tableName = "program_exercises")
public class Program_exercise {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Program program;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Exercise exercise;
    @DatabaseField
    private int iteration;
    @DatabaseField
    private int value;

    public Program_exercise() { }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public Program getProgram() {
        return program;
    }
    public void setProgram(Program program) {
        this.program = program;
    }
    public Exercise getExercise() {
        return exercise;
    }
    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }
    public int getIteration() {
        return iteration;
    }
    public void setIteration(int iteration) {
        this.iteration = iteration;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
