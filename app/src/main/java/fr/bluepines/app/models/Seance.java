package fr.bluepines.app.models;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import fr.bluepines.app.ExerciceAdapter;
import fr.bluepines.app.ProgramAdapter;
import fr.bluepines.app.SingletonProgList;

/**
 * Created by Jérémie on 31/05/2014.
 * This is a group of Programs
 */
public class Seance implements Parcelable {
    List<Pair> pg_list;
    List<Program> real_pg;
    Stack<Trip> rem_list;
    List<Name_pair> remember;

    Pair current = null;
    String name;

    ExerciceAdapter e_adapt;
    ProgramAdapter pg_adapt;

    ListView exo_list_adapted;

    class Pair {
        int n;
        Program pg;

        public Pair(int place, Program p) {
            n = place;
            pg = p;
        }
    }

    class Name_pair {
        String name;
        String comm;

        public Name_pair(String na, String c) {
            name = na;
            comm = c;
        }
    }

    class Trip {
        int l_place;
        Exercise ex;
        int p_place;

        public Trip(int list_place, Exercise pp, int pg_place) {
            l_place = list_place;
            ex = pp;
            p_place = pg_place;
        }
    }

    /**
     * The list given in a parameter can be an empty list, but can't be null
     * We store the history and the programs to be done
     *
     * @param l_init
     */
    public Seance(List<Program> l_init, String s_name) {
        init(l_init);
        name = s_name;
    }

    private void init(List<Program> base) {
        pg_list = new ArrayList<Pair>();
        real_pg = new ArrayList<Program>();
        remember = new ArrayList<Name_pair>();
        rem_list = new Stack<Trip>();

        for (Program p : base) {
            real_pg.add(p);
            pg_list.add(new Pair(remember.size(), p));
            remember.add(new Name_pair(p.getName(), p.getDescription()));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String ne) {
        name = ne;
    }

    public int getSize() {
        return pg_list.size();
    }

    public Program curr() {
        if (current == null)
            return null;
        return current.pg;
    }

    public void setExAdapter(ListView lv, Activity act) {
        exo_list_adapted = lv;
        if (current == null)
            return;
        e_adapt = new ExerciceAdapter(act, current.pg.getExerciseList());
        exo_list_adapted.setAdapter(e_adapt);
    }

    public void setPgAdapter(ListView lv, Activity act) {
        pg_adapt = new ProgramAdapter(act, real_pg);
        lv.setAdapter(pg_adapt);
    }

    public List<Program> getPgList() {
        List<Program> pg = new ArrayList<Program>();
        if (current != null)
            pg.add(current.pg.clone());
        for (Program p : real_pg)
            pg.add(p.clone());
        return pg;
    }

    public void removePg(int p) {
        if (p == -1) {
            current.pg.getExerciseList().clear();
            start(0);
            return;
        }
        real_pg.remove(p);
        pg_list.remove(p);
    }

    /**
     * Start the program at X in the program list.
     *
     * @param x
     */
    public void start(int x) {
        if (x >= pg_list.size()) {
            current = null;
            return;
        }

        if (current != null) {
            Pair p = pg_list.get(x);
            if (current.pg.getExerciseList().size() != 0) {
                Program nprog = new Program(current.pg.getName(), current.pg.getDescription());
                nprog.addExos(current.pg.getExerciseList());
                pg_list.add(0, new Pair(current.n, nprog));
                real_pg.add(0, current.pg);
                x++;
            }
            current.pg = p.pg;
            current.n = p.n;
        } else
            current = pg_list.get(x);

        pg_list.remove(x);
        real_pg.remove(x);

        if (pg_adapt != null)
            pg_adapt.notifyDataSetChanged();

        if (e_adapt != null)
            setExAdapter(exo_list_adapted, (Activity) e_adapt.getContext());
    }

    public void addProgram(Program prog) {
        Program pg = prog.clone();
        pg_list.add(new Pair(remember.size(), pg));
        remember.add(new Name_pair(pg.getName(), pg.getDescription()));
        real_pg.add(pg);
        if (current == null)
            start(0);
        if (pg_adapt != null)
            pg_adapt.notifyDataSetChanged();
    }

    /**
     * Remove the exercise in the current selection a X
     *
     * @param x the place of the exercise to be removed
     */
    public void removeExo(int x) {
        Exercise cexo = current.pg.getExo(x);
        if (cexo != null) {
            SingletonProgList.addDone(cexo);
            rem_list.push(new Trip(x, cexo, current.n));
            current.pg.getExerciseList().remove(x);
            e_adapt.notifyDataSetChanged();
        }

        if (curr().getExerciseList().size() == 0)
            start(0);
    }

    /**
     * Restore the last removed exercise
     */
    public void restoreFront() {
        if (rem_list.size() == 0)
            return;
        Trip t = rem_list.pop();

        // If the current program is the one the exercise is from, insert
        if (current != null && current.n == t.p_place) {
            current.pg.addExo(t.l_place, t.ex);
            e_adapt.notifyDataSetChanged();
        } else {
            // If not, find the program it is from and insert in original place
            for (int i = 0; i < pg_list.size(); i++)
                if (pg_list.get(i).n == t.p_place) {
                    pg_list.get(i).pg.getExerciseList().add(t.l_place, t.ex);
                    real_pg.get(i).getExerciseList().add(t.l_place, t.ex);
                    return;
                }
            // If the program is not in the list, insert a new program with the exercise
            Program p = new Program(remember.get(t.p_place).name, remember.get(t.p_place).comm);
            p.addExo(t.ex);
            pg_list.add(0, new Pair(t.p_place, p));
            real_pg.add(0, p);
            pg_adapt.notifyDataSetChanged();
            if (current == null)
                start(0);
        }
    }

    /*
    ** Parcelable implementation
    */

    public int describeContents() {
        return 0;
    }

    /**
     * This is a must have to implement Parcelable, this is the function used to write data in the stream
     *
     * @param out   The parcel to be written in
     * @param flags don't know
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(name);
        List<Program> pg = new ArrayList<Program>();
        if (current != null)
            pg.add(0, current.pg);
        for (Pair p : pg_list)
            pg.add(p.pg);

        out.writeList(pg);
    }

    /**
     * Given a Parcel, this is the function used to get the original object back
     *
     * @param in The Parcel containing the original object
     */
    private Seance(Parcel in) {
        name = in.readString();
        List<Program> pg = in.readArrayList(Program.class.getClassLoader());
        init(pg);
        start(0);
    }

    /**
     * Special structure, used to implement parcelable
     */
    public static final Parcelable.Creator<Seance> CREATOR = new Parcelable.Creator<Seance>() {
        public Seance createFromParcel(Parcel in) {
            return new Seance(in);
        }

        public Seance[] newArray(int size) {
            return new Seance[size];
        }
    };
}
