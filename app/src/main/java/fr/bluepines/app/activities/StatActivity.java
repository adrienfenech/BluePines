package fr.bluepines.app.activities;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Pair;
import android.view.SurfaceView;
import android.view.View;

import java.util.List;

import fr.bluepines.app.R;
import fr.bluepines.app.SingletonProgList;
import fr.bluepines.app.models.Exercise;

/**
 * Created by TaTa_HenRieTTe on 23/04/14.
 */
public class StatActivity extends Activity {

    SurfaceView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);
        sv = (SurfaceView) findViewById(R.id.stats_surfaceView);
    }

    public void iterDraw(View v) {
        Canvas c = getCanvas();
        if (c == null)
            return;

        Paint p_s = createMyPaint(Color.BLACK, 2, Paint.Style.FILL, 13);

        List<Pair<Time, Exercise>> lint = SingletonProgList.done;

        if (lint.size() < 2) {
            sv.getHolder().unlockCanvasAndPost(c);
            return;
        }

        float modifX = ((lint.get(lint.size() - 1).first.toMillis(true) - lint.get(0).first.toMillis(true)) / 1000) % 10000000;
        modifX = (c.getWidth() - 50) / modifX;

        int last = 0;
        int max = 0;
        int min = 0;
        // We are forced to do it, we can't guess max and min and we need them
        for (int i = 0; i < lint.size(); i++) {
            if (max < lint.get(i).second.getIter())
                max = lint.get(i).second.getIter();
            if (min > lint.get(i).second.getIter())
                min = lint.get(i).second.getIter();
        }

        float modifY = c.getHeight();
        if (max + max / 10 - min > 0)
            modifY = c.getHeight() / (max + max / 10 - min);
        else
            modifY = c.getHeight() / (max - min + 1);

        Path path = new Path();
        path.moveTo(20, c.getHeight() - (lint.get(0).second.getIter() - min) * modifY - max / 10);
        c.drawText("" + lint.get(0).first.format("%T"), 0, c.getHeight() - 5, p_s);
        c.drawText("" + lint.get(0).first.format("%x"), 0, c.getHeight() - 18, p_s);

        String last_date = lint.get(0).first.format("%x");

        for (int i = 1; i < lint.size(); i++) {

            Time t = new Time();
            t.set(lint.get(i).first.toMillis(true) - lint.get(0).first.toMillis(true));
            int a = (int) (((t.toMillis(true) / 1000) % 10000000) * modifX);

            path.lineTo(a + 20, c.getHeight() - (lint.get(i).second.getIter() - min) * modifY - max / 10);

            if (a - 20 < last + 10)
                continue;
            c.drawText("" + lint.get(i).first.format("%T"), a - 20, c.getHeight() - 5, p_s);

            String new_date = lint.get(i).first.format("%x");
            if (new_date.equals(last_date))
                continue;
            c.drawText("" + new_date, a - 20, c.getHeight() - 18, p_s);
            last = a;
            last_date = new_date;
        }

        post(c, max, path, modifY);
    }

    public void poidsDraw(View v) {
        Canvas c = getCanvas();
        if (c == null)
            return;

        Paint p_s = createMyPaint(Color.BLACK, 2, Paint.Style.FILL, 13);

        List<Pair<Time, Exercise>> lint = SingletonProgList.done;

        if (lint.size() < 2) {
            sv.getHolder().unlockCanvasAndPost(c);
            return;
        }

        float modifX = ((lint.get(lint.size() - 1).first.toMillis(true) - lint.get(0).first.toMillis(true)) / 1000) % 10000000;
        modifX = (c.getWidth() - 50) / modifX;

        int last = 0;
        int max = 0;
        int min = 0;
        // We are forced to do it, we can't guess max and min and we need them
        for (int i = 0; i < lint.size(); i++) {
            if (max < lint.get(i).second.getValue())
                max = lint.get(i).second.getValue();
            if (min > lint.get(i).second.getValue())
                min = lint.get(i).second.getValue();
        }

        float modifY = c.getHeight();
        if (max - 20 - min > 0)
            modifY = c.getHeight() / (max + 20 - min);
        else
            modifY = c.getHeight() / (max - min + 1);

        Path path = new Path();
        path.moveTo(20, c.getHeight() - lint.get(0).second.getValue() * modifY - 20);
        c.drawText("" + lint.get(0).first.format("%T"), 0, c.getHeight() - 5, p_s);
        c.drawText("" + lint.get(0).first.format("%x"), 0, c.getHeight() - 18, p_s);

        String last_date = lint.get(0).first.format("%x");

        for (int i = 1; i < lint.size(); i++) {

            Time t = new Time();
            t.set(lint.get(i).first.toMillis(true) - lint.get(0).first.toMillis(true));
            int a = (int) (((t.toMillis(true) / 1000) % 10000000) * modifX);

            path.lineTo(a + 20, c.getHeight() - lint.get(i).second.getValue() * modifY - 20);

            if (a - 20 < last + 10)
                continue;
            c.drawText("" + lint.get(i).first.format("%T"), a - 20, c.getHeight() - 5, p_s);

            String new_date = lint.get(i).first.format("%x");
            if (new_date.equals(last_date))
                continue;
            c.drawText("" + new_date, a - 20, c.getHeight() - 18, p_s);
            last = a;
            last_date = new_date;
        }

        post(c, max, path, modifY);
    }

    private Canvas getCanvas() {
        Canvas c = sv.getHolder().lockCanvas();
        if (c == null)
            return null;
        c.drawColor(Color.WHITE);
        return c;
    }

    private void post(Canvas c, int max, Path p, float modifY) {
        Paint p_s = createMyPaint(Color.BLACK, 2, Paint.Style.FILL, 13);
        Paint p_l = createMyPaint(Color.RED, 5, Paint.Style.STROKE, 10);

        c.drawText("" + max, 0, c.getHeight() - max * modifY, p_s);
        c.drawText("" + (max * 3 / 4), 0, c.getHeight() - (max * 3 / 4) * modifY, p_s);
        c.drawText("" + (max / 2), 0, c.getHeight() - (max / 2) * modifY, p_s);
        c.drawText("" + (max / 4), 0, c.getHeight() - (max / 4) * modifY, p_s);
        c.drawPath(p, p_l);
        sv.getHolder().unlockCanvasAndPost(c);
    }

    private Paint createMyPaint(int color, int width, Paint.Style style, int size) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStrokeWidth(width);
        paint.setStyle(style);
        paint.setTextSize(size);

        return paint;
    }
}
