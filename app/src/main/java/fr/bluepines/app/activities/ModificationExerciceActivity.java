package fr.bluepines.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import fr.bluepines.app.ExerciceAdapter;
import fr.bluepines.app.R;
import fr.bluepines.app.models.Program;

/**
 * Created by TaTa_HenRieTTe on 01/06/2014.
 */
public class ModificationExerciceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_exercice_modification);
    }
}
