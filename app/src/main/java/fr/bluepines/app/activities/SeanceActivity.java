package fr.bluepines.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import fr.bluepines.app.ProgramAdapter;
import fr.bluepines.app.R;
import fr.bluepines.app.SingletonProgList;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.models.Seance;

/**
 * Created by Jérémie on 25/05/2014.
 * This is the seance being done
 */
public class SeanceActivity extends Activity {
    Seance seance;
    ProgramAdapter pg_adapt;

    TextView current_exo;
    TextView waiting_exo;
    ListView current_list;
    ListView next_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice);

        // Get the program sent in parameter, thanks to Parcelable
        seance = getIntent().getParcelableExtra("Seance");

        current_exo = (TextView) findViewById(R.id.current_exo);
        current_list = (ListView) findViewById(R.id.list_current);
        waiting_exo = (TextView) findViewById(R.id.waiting_exo);
        next_list = (ListView) findViewById(R.id.list_next);

        /*current_list.setPadding(getResources().getDisplayMetrics().widthPixels / 4,
                current_list.getPaddingTop(),
                current_list.getPaddingRight(),
                current_list.getPaddingBottom());*/

        current_list.setOnTouchListener(new AdapterView.OnTouchListener() {
                                            float historicX = Float.NaN;
                                            final float delta = 100;
                                            int current_int = -1;

                                            @Override
                                            public boolean onTouch(View view, MotionEvent e) {
                                                switch (e.getAction()) {
                                                    case MotionEvent.ACTION_MOVE:
                                                        View v = ((ListView) view).getChildAt(current_int);
                                                        if (v == null)
                                                            return true;
                                                        v.setLeft((int) (e.getX() - historicX));
                                                        if (e.getX() - historicX >= delta
                                                                || e.getX() - historicX <= -delta) {
                                                            v.setBackgroundColor(Color.RED);
                                                        }
                                                        else
                                                            v.setBackgroundColor(Color.GREEN);
                                                        break;
                                                    case MotionEvent.ACTION_DOWN:
                                                        historicX = e.getX();
                                                        Rect rect = new Rect();
                                                        int childCount = ((ListView) view).getChildCount();
                                                        int[] listViewCoords = new int[2];
                                                        view.getLocationOnScreen(listViewCoords);
                                                        int x = (int) e.getRawX() - listViewCoords[0];
                                                        int y = (int) e.getRawY() - listViewCoords[1];
                                                        View child;
                                                        for (int i = 0; i < childCount; i++) {
                                                            child = ((ListView) view).getChildAt(i);
                                                            child.getHitRect(rect);
                                                            if (rect.contains(x, y)) {
                                                                current_int = i;
                                                                return true;
                                                            }
                                                        }
                                                        break;
                                                    case MotionEvent.ACTION_UP:
                                                        View vue = ((ListView) view).getChildAt(current_int);
                                                        if (vue == null)
                                                            return true;
                                                        vue.setLeft(0);
                                                        vue.setBackgroundColor(Color.TRANSPARENT);
                                                        if (e.getX() - historicX < delta
                                                                && e.getX() - historicX > -delta)
                                                            return true;
                                                        final int toDel = current_int;
                                                        runOnUiThread(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                seance.removeExo(toDel);
                                                                update_size();
                                                            }
                                                        });
                                                        return true;

                                                    default:
                                                        return false;
                                                }
                                                return false;
                                            }
                                        }
        );

        next_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        seance.start(i);
                        update_size();
                    }
                });
            }
        });
        update();
    }

    private void update_size() {
        if (seance.curr() != null && seance.curr().getExerciseList().size() != 0)
            current_exo.setText(seance.curr().getName() + " (" + seance.curr().getExerciseList().size() + ")");
        else
            current_exo.setText("Choisir un programme pour commencer");
        waiting_exo.setText("Exercices en attente : " + seance.getSize());
    }

    private void update() {
        seance.setExAdapter(current_list, this);
        seance.setPgAdapter(next_list, this);

        update_size();
    }

    public void restore(View view) {
        seance.restoreFront();
        update_size();
    }

    /**
     * Save the current seance.
     *
     * @param view
     */
    public void save(View view) {
        // History is not kept
        SingletonProgList.newSeance(seance);
        final Dialog dialog = new Dialog(view.getContext(), android.R.style.Theme_Holo_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_change_value);
        (dialog.findViewById(R.id.dialog_number_picker)).setVisibility(View.INVISIBLE);
        ((TextView) dialog.findViewById(R.id.dialog_text)).setText("Séance mémorisée !");
        ((Button) dialog.findViewById(R.id.dialog_button)).setText("Génial !");

        Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Add a program to list
     *
     * @param view
     */
    public void addProgBut(View view) {
        // History is not kept
        final Dialog dialog = new Dialog(view.getContext(), android.R.style.Theme_Holo_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_list);

        ListView list = (ListView) dialog.findViewById(R.id.dialog_list);

        final ProgramAdapter adapter = new ProgramAdapter(this, SingletonProgList.programs);
        list.setAdapter(adapter);

        Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                List<Program> pgs = adapter.getSelected();
                adapter.reinitCheck();
                for (Program p : pgs)
                    seance.addProgram(p);
                update();
            }
        });
        dialog.show();
    }

    /**
     * Remove a program from list
     *
     * @param view
     */
    public void removeProgBut(View view) {
        // History is not kept
        final Dialog dialog = new Dialog(view.getContext(), android.R.style.Theme_Holo_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_list);

        ListView list = (ListView) dialog.findViewById(R.id.dialog_list);

        final ProgramAdapter adapter = new ProgramAdapter(this, seance.getPgList());
        list.setAdapter(adapter);

        Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                List<Integer> pgs = adapter.getSelectedInts();
                adapter.reinitCheck();
                for (int i = pgs.size() - 1; i >= 0; i--)
                    seance.removePg(pgs.get(i) - 1);
                update();
            }
        });
        dialog.show();
    }
}
