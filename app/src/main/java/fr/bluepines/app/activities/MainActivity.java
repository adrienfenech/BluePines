package fr.bluepines.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fr.bluepines.app.DBManager;
import fr.bluepines.app.ProgramAdapter;
import fr.bluepines.app.R;
import fr.bluepines.app.SingletonProgList;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.models.Seance;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create the Singleton for the DBManager
        DBManager.init(this);

        // Creates the singleton and initializes it
        SingletonProgList.getInstance();
        List<Program> programs = DBManager.getInstance().getAllPrograms();
        SingletonProgList.copy(programs);

        // We initialize the list, to be replaced by the DB call.
        SingletonProgList.copySeance(new ArrayList<Seance>());

        // Init the table of exercises
        DBManager.getInstance().initExercises();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        final Activity mainActivity = this;
        getMenuInflater().inflate(R.menu.main, menu);
        ((MenuItem) menu.findItem(R.id.action_help)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                final Dialog dialog = new Dialog(mainActivity, android.R.style.Theme_Holo_Dialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_help);

                ListView lv = (ListView) dialog.findViewById(R.id.help_list);
                List<Program> lp = new ArrayList<Program>();
                lp.add(new Program("Ceci ne fait rien", "Rien du tout"));
                ProgramAdapter pa = new ProgramAdapter(mainActivity, lp);
                lv.setAdapter(pa);

                ((Button) dialog.findViewById(R.id.help_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void newSeance(View view) {
        Intent intent = new Intent(this, NewSeanceActivity.class);
        startActivity(intent);
    }

    public void program(View view) {
        Intent intent = new Intent(this, ProgramActivity.class);
        startActivity(intent);
    }

    public void stat(View view) {
        Intent intent = new Intent(this, StatActivity.class);
        startActivity(intent);
    }

    public void calendar(View view) {
        Intent intent = new Intent(this, CalendarActivity.class);
        startActivity(intent);
    }
}
