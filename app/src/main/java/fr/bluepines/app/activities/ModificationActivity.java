package fr.bluepines.app.activities;


import fr.bluepines.app.DBManager;
import fr.bluepines.app.ExpandableListAdapter;
import fr.bluepines.app.models.Exercise;
import fr.bluepines.app.ExerciceAdapter;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.R;
import fr.bluepines.app.models.Set;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.app.Dialog;
import android.content.Intent;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.view.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

/**
 * Created by Jérémie on 04/05/2014.
 */
public class ModificationActivity extends Activity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Program pg;
    Exercise toAdd;
    Dialog dialog;
    Dialog dialog_2;
    EditText d_1;
    EditText d_2;
    ListView lvListe;
    private ExerciceAdapter adapter;
    String itemSelected;
    List<Exercise> exercises;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_modification);

        // Get the program sent in parameter, thanks to Parcelable
        // We get the program with the name I gave in the calling function
        itemSelected = null;
        pg = getIntent().getParcelableExtra("Program");
        setTitle("Modifier " + pg.getName());
        ((TextView)findViewById(R.id.modif_name)).setText(pg.getName());
        ((TextView)findViewById(R.id.modif_comment)).setText(pg.getDescription());

        lvListe = (ListView) findViewById(R.id.modif_list);

        // Adding an exercice to the list, for testing purposes
        // Initializes the list
        if (pg.getExerciseList().isEmpty())
            pg.initExerciseList();
        adapter = new ExerciceAdapter(this, pg.getExerciseList());
        lvListe.setAdapter(adapter);
        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int i, long mylng) {
                // Starts activity for program modification

                Exercise selectedExercise = adapter.getItem(i);
                Intent intent = new Intent(adapter.getContext(), ModificationExerciceActivity.class);

                // See NewSeanceActivity for information
                intent.putExtra("Exercise", selectedExercise);
                startActivity(intent);
            }
        });


        dialog = new Dialog(this, android.R.style.Theme_Holo_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_exercice);
    }

    private boolean alreadyExist(String test, List<String> listDataHeader) {
        for (int i = 0; i < listDataHeader.size(); i++)
            if (test.equals(listDataHeader.get(i)))
                return true;
        return false;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        ArrayList<ArrayList<String>> listTEMP = new ArrayList<ArrayList<String>>();

        exercises = DBManager.getInstance().getAllExercises();
        exercises.add(new Exercise("Développer-coucher_1", "Muscle tes muscles, sisi", "Kg", "Haut du corps"));
        exercises.add(new Exercise("Développer-coucher_2", "Muscle tes muscles, sisi", "Kg", "Haut du corps"));
        exercises.add(new Exercise("Développer-coucher_3", "Muscle tes muscles, sisi", "Kg", "Haut du corps"));
        exercises.add(new Exercise("Développer-coucher_4", "Muscle tes muscles, sisi", "Kg", "Haut du corps"));
        exercises.add(new Exercise("Chaise_1", "Muscle tes muscles, sisi", "Sec", "Jambes"));
        exercises.add(new Exercise("Chaise_2", "Muscle tes muscles, sisi", "Sec", "Jambes"));
        exercises.add(new Exercise("Chaise_3", "Muscle tes muscles, sisi", "Sec", "Jambes"));
        exercises.add(new Exercise("Chaise_4", "Muscle tes muscles, sisi", "Sec", "Jambes"));

        for (int i = 0; i < exercises.size(); i++) {
            String head = exercises.get(i).getCategory();
            if (!alreadyExist(head, listDataHeader)) {
                listDataHeader.add(head);
                listTEMP.add(new ArrayList<String>());
            }
        }

        for (int i = 0; i < exercises.size(); i++) {
            for (int j = 0; j < listDataHeader.size(); j++) {
                if (exercises.get(i).getCategory().equals(listDataHeader.get(j)))
                    listTEMP.get(j).add(exercises.get(i).getName());
            }
        }

        for (int i = 0; i < listDataHeader.size(); i++) {
            listDataChild.put(listDataHeader.get(i), listTEMP.get(i));
        }


        // Adding child data
        //listDataHeader.add("Top 250");
        //listDataHeader.add("Now Showing");
        //listDataHeader.add("Coming Soon..");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        //listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        //listDataChild.put(listDataHeader.get(1), nowShowing);
        //listDataChild.put(listDataHeader.get(2), comingSoon);
    }

    public void addExerciseButton(View view) {
        // Be sure that the dialog is cleanup
        //exerciseName.setText(null);
        Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
        btnAccept.setText(getString(R.string.ajouter));
        itemSelected = null;
        // get the listview
        //expListView = new ExpandableListView(this);
        expListView = (ExpandableListView) dialog.findViewById(R.id.lvExp);
        // preparing list data
        prepareListData();


        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        // setting list adapter
        if (listAdapter != null)
            expListView.setAdapter(listAdapter);


        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                 Toast.makeText(getApplicationContext(),
                 "Catégorie : " + listDataHeader.get(groupPosition),
                 Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                itemSelected = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + itemSelected, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });



        btnAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (itemSelected.length() > 1) {
                    boolean alreadyExit = false;
                    if (!alreadyExit) {

                        addExercise(itemSelected);
                        dialog.dismiss();
                    } else
                        Toast.makeText(getApplicationContext(),
                            itemSelected + " : Exercice non valide !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (exercises.size() == 0) {
            Toast.makeText(getApplicationContext(),
                    "Aucun exercice trouvé dans la base de données",
                    Toast.LENGTH_SHORT).show();
        }
        else
            dialog.show();
    }

    public void addExercise(String name) {

        for (int i = 0; i < exercises.size(); i++) {
            if (name.equals(exercises.get(i).getName())) {
                toAdd = exercises.get(i);

                dialog.dismiss();
                dialog_2 = new Dialog(this, android.R.style.Theme_Holo_Dialog);
                dialog_2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_2.setCancelable(true);
                dialog_2.setContentView(R.layout.dialog_mod_exercice);

                Button btnAccept = (Button) dialog_2.findViewById(R.id.button);
                btnAccept.setText("Valider");

                d_1 = (EditText) dialog_2.findViewById(R.id.editText);
                d_2 = (EditText) dialog_2.findViewById(R.id.editText2);
                d_2.setHint("Valeur (en " + exercises.get(i).getUnit() + " )");

                btnAccept.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (Pattern.matches("[0-9]+", d_1.getText()) && Pattern.matches("[0-9]+", d_2.getText())) {
                            toAdd.setSet(new Set((Integer.parseInt(d_1.getText().toString())), (Integer.parseInt(d_2.getText().toString()))));
                            pg.addExo(toAdd);
                            dialog_2.dismiss();
                        }
                        else
                            Toast.makeText(getApplicationContext(),
                            "Les valeurs rentrées sont invalides",
                            Toast.LENGTH_SHORT).show();
                    }
                });

                dialog_2.show();
            }
        }
        //Program program = new Program(name, comment);

        //// Added in database
        //db.createProgram(program, 1);
        //// TODO : check if the program has been added

        //adapter.add(program);
        //adapter.notifyDataSetChanged();
    }
}