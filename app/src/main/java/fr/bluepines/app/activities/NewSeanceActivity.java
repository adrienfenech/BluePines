package fr.bluepines.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import fr.bluepines.app.R;
import fr.bluepines.app.SeanceAdapter;
import fr.bluepines.app.SingletonProgList;
import fr.bluepines.app.models.Seance;


public class NewSeanceActivity extends Activity {
    ListView lvListe;
    SeanceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_new_seance);
        setContentView(R.layout.activity_new_seance);
        lvListe = (ListView) findViewById(R.id.listView);

        List<Seance> seances = SingletonProgList.seances;
        adapter = new SeanceAdapter(this, seances);
        lvListe.setAdapter(adapter);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int i, long mylng) {
                // Starts activity for program modification
                Seance selectedSeance = (Seance) adapter.getItem(i);
                Intent intent = new Intent(getBaseContext(), SeanceActivity.class);

                // We put 'extra' on something like a container. This is the only way I found
                // To give information from an activity to another being called.
                // We will get the object back by looking for the name "Seance"
                intent.putExtra("Seance", selectedSeance);
                startActivity(intent);
            }
        });

        lvListe.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Seance selectedSeance = adapter.getItem(i);
                final Dialog dialog = new Dialog(adapter.getContext(), android.R.style.Theme_Holo_Dialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_program);

                dialog.findViewById(R.id.editText2).setVisibility(View.INVISIBLE);
                ((EditText) dialog.findViewById(R.id.editText)).setHint("Nouveau nom");
                Button btnAccept = (Button) dialog.findViewById(R.id.button);
                btnAccept.setText(getString(R.string.modifier));
                btnAccept.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        selectedSeance.setName(((EditText) dialog.findViewById(R.id.editText)).getText().toString());
                        dialog.dismiss();
                        adapter.notifyDataSetChanged();

                        //TODO update database.
                    }
                });

                dialog.show();
                return true;
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }
}
