package fr.bluepines.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import fr.bluepines.app.DBManager;
import fr.bluepines.app.DatabaseHelper;
import fr.bluepines.app.ProgramAdapter;
import fr.bluepines.app.models.Program;
import fr.bluepines.app.R;
import fr.bluepines.app.SingletonProgList;

import static android.content.DialogInterface.OnClickListener;

/**
 * Created by TaTa_HenRieTTe on 23/04/14.
 */
public class ProgramActivity extends Activity {

    ListView lvListe;
    Dialog dialog;
    EditText programName;
    EditText programComment;
    long programId;
    // DatabaseHelper help to dialog with the DataBase
    ProgramAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_program);
        lvListe = (ListView) findViewById(R.id.listView);

        adapter = new ProgramAdapter(this, SingletonProgList.programs);
        lvListe.setAdapter(adapter);

        // Quick click -> modification activity, for modifying exercises and other information
        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int i, long mylng) {
                // Starts activity for program modification
                Program selectedProgram = adapter.getItem(i);
                Intent intent = new Intent(adapter.getContext(), ModificationActivity.class);

                // See NewSeanceActivity for information
                intent.putExtra("Program", selectedProgram);
                startActivity(intent);
            }
        });

        // Long click, for modifying the name and the comment
        lvListe.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Program selectedProgram = adapter.getItem(i);
                Button btnAccept = (Button) dialog.findViewById(R.id.button);
                btnAccept.setText(getString(R.string.modifier));
                btnAccept.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        // Programme EDIT => Update the database
                        if (programName.getText().toString().length() > 1) {
                            boolean alreadyExit = false;
                            if (!alreadyExit) {
                                updateProgram(programName.getText().toString(), programComment.getText().toString());
                                programName.setText(null);
                                programComment.setText(null);
                                dialog.dismiss();
                            } else
                                programName.setText("Nom de programme invalid");
                        }
                    }
                });

                programName.setText(selectedProgram.getName());
                programComment.setText(selectedProgram.getDescription());
                programId = selectedProgram.getId();

                dialog.show();
                return true;
            }
        });

        dialog = new Dialog(this, android.R.style.Theme_Holo_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_program);
        programName = (EditText) dialog.findViewById(R.id.editText);
        programComment = (EditText) dialog.findViewById(R.id.editText2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_seance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addProgramButton(View view) {
        // Be sure that the dialog is cleanup
        programName.setText(null);
        programComment.setText(null);

        Button btnAccept = (Button) dialog.findViewById(R.id.button);
        btnAccept.setText(getString(R.string.ajouter));
        btnAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (programName.getText().toString().length() > 1) {
                    boolean alreadyExit = false;
                    if (!alreadyExit) {
                        addProgram(programName.getText().toString(), programComment.getText().toString());
                        programName.setText(null);
                        programComment.setText(null);
                        dialog.dismiss();
                    } else
                        programName.setText("Nom de programme invalide");
                }
            }
        });
        dialog.show();
    }

    public void delProgramsButton(View view) {
        final List<Program> selected = adapter.getSelected();

        // Pop up an alert dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog);
        alertDialog.setCancelable(true);
        alertDialog.setMessage("Supprimer " + selected.size() + " programme(s) ?");
        alertDialog.setNegativeButton("Non", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.setPositiveButton("Oui", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                // Remove from the DB
                DBManager.getInstance().removePrograms(selected);

                // TODO : check if the programs has been deleted

                // Remove in the adapter
                for (Program p : selected)
                    adapter.remove(p);
                adapter.notifyDataSetChanged();
            }
        });
        alertDialog.show();
    }

    static int nb = 1;

    public void addProgram(String name, String comment) {
        Program program = new Program(name, comment);

        // Added in database
        DBManager.getInstance().createProgram(program);

        adapter.add(program);
        adapter.notifyDataSetChanged();
    }

    public void updateProgram(String name, String comment) {
        Program program = new Program(name, comment);
        program.setId(programId);

        // Update the program in DB
        DBManager.getInstance().updateProgram(program);
        // TODO : check if the program has been added

        adapter.updateProgram(program);
        adapter.notifyDataSetChanged();
    }
}
