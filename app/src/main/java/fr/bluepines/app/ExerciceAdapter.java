package fr.bluepines.app;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.bluepines.app.models.Exercise;

/**
 * Created by Jérémie on 25/05/2014.
 */
public class ExerciceAdapter extends ArrayAdapter<Exercise> {
    private List<Exercise> exercises;
    private final Activity context;

    static class ViewHolder {
        TextView exerciceName;
        TextView exerciceDescription;
        Button b1;
        Button b2;
    }

    public ExerciceAdapter(Activity context, List<Exercise> exercises) {
        super(context, R.layout.buttons_list, exercises);
        this.exercises = exercises;
        this.context = context;
    }

    public List<Exercise> getSelected() {
        List<Exercise> result = new ArrayList<Exercise>();
        for (Exercise e : exercises)
            if (e.isSelected())
                result.add(e);

        return result;
    }

    public void removeAt(int x) {
        exercises.remove(x);
    }

    public void addAt(Exercise ex, int x) {
        exercises.add(x, ex);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            final ViewHolder holder = new ViewHolder();
            view = inflater.inflate(R.layout.buttons_list, null);

            holder.exerciceName = (TextView) view.findViewById(R.id.name);
            holder.exerciceDescription = (TextView) view.findViewById(R.id.description);
            holder.b1 = (Button) view.findViewById(R.id.list_button1);
            holder.b2 = (Button) view.findViewById(R.id.list_button2);

            holder.b1.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final Dialog dialog = new Dialog(context, android.R.style.Theme_Holo_Dialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.dialog_change_value);

                    final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.dialog_number_picker);
                    np.setMinValue(0);
                    np.setMaxValue(100);
                    np.setValue(exercises.get(position).getIter());

                    Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
                    btnAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            exercises.get(position).setIter(np.getValue());
                            holder.b1.setText("" + np.getValue());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    return true;
                }
            });

            holder.b2.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final Dialog dialog = new Dialog(context, android.R.style.Theme_Holo_Dialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.dialog_change_value);

                    final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.dialog_number_picker);
                    np.setMinValue(0);
                    np.setMaxValue(100);
                    np.setValue(exercises.get(position).getValue());

                    Button btnAccept = (Button) dialog.findViewById(R.id.dialog_button);
                    btnAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            exercises.get(position).setValue(np.getValue());
                            holder.b2.setText("" + np.getValue());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    return true;
                }
            });

            view.setTag(holder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.exerciceName.setText(exercises.get(position).getName());
        holder.exerciceDescription.setText(exercises.get(position).getDescription());

        if (exercises.get(position).getCurrent() != null) {
            holder.b1.setText("" + exercises.get(position).getIter());
            holder.b2.setText("" + exercises.get(position).getValue());
        }

        return view;
    }
}
